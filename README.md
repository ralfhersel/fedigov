# Fedigov - Public communication for authorities

A webpage to promote public communication and federated services for public authorities and organizations.

![Fedigov-Logo](static/images/logos/fedigov_logo.svg)

This is an initiative of [GNU/Linux.ch](https://gnulinux.ch) and the [Free Software Foundation Europe](https://fsfe.org) - Country Group Switzerland.

## Summary
Are your public institutions still stuck in proprietary social media networks? Encourage them to start using federated social media as well. Explain to local authorities the importance of sovereign communication at the public sector. Use the letter template and send it your local authorities asking them to join federated social media.

[FediGov-Website](https://fedigov.ch/en/)

## Press Releases
#### (German)

PRESSEMITTEILUNG

Do, 3. November 2022

*GNU/Linux.ch startet zusammen mit der FSFE-Schweiz eine Kampagne, um öffentliche Institutionen zu souveräner Kommunikation zu bewegen.*

Soziale Netzwerke werden auch für öffentliche Einrichtungen und Institutionen immer wichtiger. Sie ermöglichen eine direkte Kommunikation mit Bürger:innen und erlauben eine zeitnahe Publikation von für die Bevölkerung relevanten Informationen.

Dabei kommen zumeist bekannte kommerzielle Plattformen wie Twitter, Facebook, YouTube oder auch TikTok zum Einsatz. Menschen, die das Internet selbstbestimmt und datenschutzfreundlich nutzen möchten, sind davon ausgeschlossen. Durch eine prominente Verlinkung und die Nutzung der kommerziellen Dienste wird darüber hinaus indirekt Werbung für die Grosskonzerne gemacht, was weitere Bürger:innen dazu bewegen könnte, entsprechende Dienste zu nutzen.

Doch Alternativen wie das Fediverse bestehen bereits heute und erfreuen sich einer wachsenden Beliebtheit, zum Beispiel bei vielen Bundesbehörden in Deutschland. Dabei handelt es sich um einen Verbund unterschiedlicher Anwendungen wie Mastodon, welche über ein gemeinsames Protokoll miteinander kommunizieren. Nutzer:innen haben die Möglichkeit, eine eigene Instanz zu betreiben, oder sich bereits bestehender Instanzen anzuschliessen.

Dadurch ergibt sich eine unabhängige und selbstbestimmte Nutzung und Gestaltung digitaler Technologien und bildet den Grundstein für unsere Demokratie in einer zunehmend digitalisierten Gesellschaft. Bürger:innen sind nicht gezwungen, ihre Daten an Grosskonzerne weiterzugeben, um mit öffentlichen Institutionen kommunizieren zu können.

Mit der Kampagne FediGov, möchten die FSFE Schweiz und GNU/Linux.ch auf die Thematik aufmerksam machen, und bietet sowohl Bürger:innen als auch Behörden direkte Handlungsmöglichkeiten. Interessiere können anhand einer Vorlage einen Brief an Behörden schicken, um diese zu motivieren, auch im Fediverse vertreten zu sein. Öffentliche Institutionen wird Hilfestellung zum Umgang mit dezentralen Sozialen Netzwerken auf Basis von Freier Software gegeben.

[FediGov-Webseite](https://fedigov.ch/)

---

#### (English)

PRESS RELEASE

Thu, November 3, 2022

*GNU/Linux.ch launches a campaign together with FSFE-Switzerland to encourage public institutions to communicate sovereignly.*

Social networks are becoming more and more important for public bodies and institutions as well. They enable a direct communication with citizens and allow a timely publication of information relevant for the population.

In most cases, well-known commercial platforms such as Twitter, Facebook, YouTube or TikTok are used. People who want to use the Internet in a self-determined and privacy-friendly way are excluded from this. Through prominent linking and the use of the commercial services, advertising is also indirectly made for the large corporations, which could encourage more citizens to use the corresponding services.

However, alternatives such as Fediverse already exist today and are enjoying growing popularity, for example with many federal authorities in Germany. This is a network of different applications such as Mastodon, which communicate with each other via a common protocol. Users have the option of operating their own instance or joining existing instances.

This results in an independent and self-determined use and design of digital technologies and forms the cornerstone for our democracy in an increasingly digitalized society. Citizens are not forced to pass on their data to large corporations in order to be able to communicate with public institutions.

With the FediGov campaign, FSFE Switzerland and GNU/Linux.ch want to raise awareness of the issue and offer both citizens and public authorities direct opportunities for action. Interested parties can use a template to send a letter to public authorities to motivate them to also be represented in Fediverse. Public institutions are given assistance in dealing with decentralized social networks based on Free Software.

[FediGov-Website](https://fedigov.ch/en/)

## Fork it

Country or local groups of the FSFE can easily fork and adapt this website to your national or local requirements. Here is a quick guideline:

1. [Install Hugo](https://gohugo.io/) on your GNU/Linux-Computer.

2. Create a new site named **fedigov** (or whatever name you like) according to the Hugo [Quick Start](https://gohugo.io/getting-started/quick-start/) guide.

3. Ensure that Git is installed on your computer. If you don't know what it is, [read this](https://www.baeldung.com/git-guide).

4. Clone this repo to your Hugo fedigov directory: **git clone https://codeberg.org/ralfhersel/fedigov.git**

5. Run the site locally: **hugo server**

6. View it in your webbrowser at: http://localhost:1313/

7. Change the content to your national/lcoal requirements.

8. Basic settings are in: **config.yaml**

9. Content is in: **i18n**

10. Images are in: **static/images**

11. If you are done with all changes, run: **hugo**

12. ..and ftp the **public** directory to your website hoster.

### Congrats, your FediGov site is live.

Next steps:

* Spread the word inside the FSFE

* Create a press release and send it to national media companies, authorities, social media, affiliates and multipliers.

* Try to convince leading national authorities to set up a Fediverse-Server for other authorities: [example from Germany](https://social.bund.de/explore)

* Encourage other local or national FSFE communities to do the same.
